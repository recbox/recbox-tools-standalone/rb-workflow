#!/usr/bin/env bash
VER="1.2.1"
# Translation
source /usr/bin/recbox-workflow-settings_lang.sh
# File paths
DAILY_CONFIG_DESKTOP_PATH="$HOME/.config/recbox-workflow-settings/daily-desktop"
DAILY_CONFIG_SERVICE_PATH="$HOME/.config/recbox-workflow-settings/daily-service"
DAILY_SYSTEM_TWEAKS_PATH="$HOME/.config/recbox-workflow-settings/daily-tweaks"
DAILY_GOVERNOR_PATH="$HOME/.config/recbox-workflow-settings/daily-governor"
STUDIO_CONFIG_DESKTOP_PATH="$HOME/.config/recbox-workflow-settings/studio-desktop"
STUDIO_CONFIG_SERVICE_PATH="$HOME/.config/recbox-workflow-settings/studio-service"
STUDIO_SYSTEM_TWEAKS_PATH="$HOME/.config/recbox-workflow-settings/studio-tweaks"
STUDIO_GOVERNOR_PATH="$HOME/.config/recbox-workflow-settings/studio-governor"
CONF_PATH="$HOME/.config/recbox-workflow-settings/conf"
ICON_PATH="/usr/share/icons/hicolor/scalable/apps/recbox-workflow-settings.svg"
# Menu widgets
SETTINGS_MAIN_MENU="zenity --list --width=544 --height=316 --ok-label=$OK_BUTTON \
    --cancel-label=$CANCEL_BUTTON --column=$OPTIONS_COLUMN_TITLE \
    --column=$SEPARATOR --column=$DESCRIPTION_COLUMN_TITLE --window-icon=$ICON_PATH"
SETTINGS_SUB_MENU="zenity --list --width=572 --height=230 --ok-label=$OK_BUTTON \
    --cancel-label=$CANCEL_BUTTON --extra-button=$EXTRA_BUTTON --column=$OPTIONS_COLUMN_TITLE \
    --column=$SEPARATOR --column=$DESCRIPTION_COLUMN_TITLE --window-icon=$ICON_PATH"
DESKTOP_MENU="zenity --list --checklist --multiple --width=400 --height=400 \
    --ok-label=$OK_BUTTON --cancel-label=$CANCEL_BUTTON --extra-button=$EXTRA_BUTTON \
    --column=$SEPARATOR --column=$OPTIONS_COLUMN_TITLE --window-icon=$ICON_PATH"
XDG_MENU="zenity --list --radiolist --width=572 --height=230 --ok-label=$OK_BUTTON \
    --extra-button=$EXTRA_BUTTON --cancel-label=$CANCEL_BUTTON --column=$SEPARATOR \
    --column=$OPTIONS_COLUMN_TITLE --window-icon=$ICON_PATH"
SERVICE_MENU="zenity --list --checklist --multiple --width=685 --height=380 --ok-label=$OK_BUTTON \
    --cancel-label=$CANCEL_BUTTON --extra-button=$EXTRA_BUTTON --column=$SEPARATOR \
    --column=$OPTIONS_COLUMN_TITLE --column=$SEPARATOR --column=$NAME_COLUMN_TITLE \
    --column=$SEPARATOR --column=$DESCRIPTION_COLUMN_TITLE --window-icon=$ICON_PATH"
TWEAKS_MENU="zenity --list --checklist --multiple --width=685 --height=380 --ok-label=$OK_BUTTON \
    --cancel-label=$CANCEL_BUTTON --extra-button=$EXTRA_BUTTON --column=$SEPARATOR \
    --column=$OPTIONS_COLUMN_TITLE --column=$SEPARATOR --column=$NAME_COLUMN_TITLE \
    --column=$SEPARATOR --column=$DESCRIPTION_COLUMN_TITLE --window-icon=$ICON_PATH"
GOVERNOR_MENU="zenity --list --radiolist --width=585 --height=290 --ok-label=$OK_BUTTON \
    --cancel-label=$CANCEL_BUTTON --extra-button=$EXTRA_BUTTON --column=$SEPARATOR \
    --column=$OPTIONS_COLUMN_TITLE --column=$SEPARATOR --column=$NAME_COLUMN_TITLE \
    --column=$SEPARATOR --column=$DESCRIPTION_COLUMN_TITLE --window-icon=$ICON_PATH"

settings() {
MAIN_BOX=$($SETTINGS_MAIN_MENU --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_TEXT" \
    "$DAILY_OPTION" "$SEPARATOR" "$DAILY_OPTION_DESC" \
    "$STUDIO_OPTION" "$SEPARATOR" "$STUDIO_OPTION_DESC" \
    "$UNLOCK_XDG" "$SEPARATOR" "$UNLOCK_XDG_DESC" \
    "$RESTART_OPTION" "$SEPARATOR" "$RESTART_OPTION_DESC" \
    "$DOCS_OPTION" "$SEPARATOR" "$DOCS_OPTION_DESC" \
    "$ABOUT_OPTION" "$SEPARATOR" "$ABOUT_DESC")
    if [[ -z $MAIN_BOX ]]; then
        exit 0
    elif [[ $MAIN_BOX == "$DAILY_OPTION" ]]; then
        if [[ -f $DAILY_CONFIG_DESKTOP_PATH ]]; then
            echo ":: $AUTOSTART_OUTPUT_CONFIGURED_DAILY"
        elif [[ ! -f $DAILY_CONFIG_DESKTOP_PATH ]]; then
            echo -e ":: $DST_OUTPUT_CONFIG_NOTEXIST\n -> $AUTOSTART_OUTPUT_CREATING_CONFIG_DAILY"
            reconf_daily_desktop
        fi
        if [[ -f $DAILY_CONFIG_SERVICE_PATH ]]; then
            echo ":: $SERVICE_OUTPUT_CONFIGURED_DAILY"
        elif [[ ! -f $DAILY_CONFIG_SERVICE_PATH ]]; then
            echo -e ":: $DST_OUTPUT_CONFIG_NOTEXIST\n -> $SERVICE_OUTPUT_CREATING_CONFIG_DAILY"
            reconf_daily_services
        fi
        if [[ -f $DAILY_SYSTEM_TWEAKS_PATH ]]; then
            echo ":: $TWEAKS_OUTPUT_CONFIGURED_DAILY"
        elif [[ ! -f $DAILY_SYSTEM_TWEAKS_PATH ]]; then
            echo -e ":: $DST_OUTPUT_CONFIG_NOTEXIST\n -> $TWEAKS_OUTPUT_CREATING_CONFIG_DAILY"
            reconf_daily_tweaks
        fi
        exec recbox-workflow-settings.sh --daily-submenu
    elif [[ $MAIN_BOX == "$STUDIO_OPTION" ]]; then
        if [[ -f $STUDIO_CONFIG_DESKTOP_PATH ]]; then
            echo ":: $AUTOSTART_OUTPUT_CONFIGURED_DAILY"
        elif [[ ! -f $STUDIO_CONFIG_DESKTOP_PATH ]]; then
            echo -e ":: $DST_OUTPUT_CONFIG_NOTEXIST\n -> $AUTOSTART_OUTPUT_CREATING_CONFIG_DAILY"
            reconf_studio_desktop
        fi
        if [[ -f $STUDIO_CONFIG_SERVICE_PATH ]]; then
            echo ":: $SERVICE_OUTPUT_CONFIGURED_DAILY"
        elif [[ ! -f $STUDIO_CONFIG_SERVICE_PATH ]]; then
            echo -e ":: $DST_OUTPUT_CONFIG_NOTEXIST\n -> $SERVICE_OUTPUT_CREATING_CONFIG_DAILY"
            reconf_studio_services
        fi
        if [[ -f $STUDIO_SYSTEM_TWEAKS_PATH ]]; then
            echo ":: $TWEAKS_OUTPUT_CONFIGURED_DAILY"
        elif [[ ! -f $STUDIO_SYSTEM_TWEAKS_PATH ]]; then
            echo -e ":: $DST_OUTPUT_CONFIG_NOTEXIST\n -> $TWEAKS_OUTPUT_CREATING_CONFIG_DAILY"
            reconf_studio_tweaks
        fi
        exec recbox-workflow-settings.sh --studio-submenu
    elif [[ $MAIN_BOX == "$UNLOCK_XDG" ]]; then
        settings_xdg_menu
    elif [[ $MAIN_BOX == "$RESTART_OPTION" ]]; then
        reconf_studio_services; reconf_daily_services; reconf_daily_tweaks; reconf_studio_tweaks; reconf_daily_governor; reconf_studio_governor
        zenity --info --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_INFO_TEXT" \
        --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
        exec recbox-workflow-settings.sh -sS
    elif [[ $MAIN_BOX == "$DOCS_OPTION" ]]; then
        xdg-open /usr/share/doc/recbox/Workflow\ documentation.pdf; recbox-workflow-settings.sh -S
    elif [[ $MAIN_BOX == "$ABOUT_OPTION" ]]; then
        zenity --info --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" \
        --width="400" --title="$ABOUT_TITLE" --text="<b>RecBox Workflow $VER</b>\n\n$ABOUT_TEXT"
        recbox-workflow-settings.sh -S
    fi
}

daily_submenu() {
BOX=$($SETTINGS_SUB_MENU --title="$DAILY_SUBMENU_TITLE" --text="$DAILY_SUBMENU_TEXT" \
    "$SETUP_DESKTOP" "$SEPARATOR" "$SETUP_DESKTOP_DESC" \
    "$SETUP_SERVICES" "$SEPARATOR" "$SETUP_SERVICES_DESC" \
    "$SYSTEM_TWEAKS" "$SEPARATOR" "$SYSTEM_TWEAKS_DESC" \
    "$GOVERNOR" "$SEPARATOR" "$GOVERNOR_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$SETUP_DESKTOP" ]]; then
        if [[ -z $(ls "$HOME/.config/autostart") ]]; then
            zenity --info  --width="400" --title="$AUTOSTART_INFO_TITLE" \
            --text="$AUTOSTART_INFO_TEXT" --icon-name="recbox-workflow-settings" \
            --window-icon="$ICON_PATH"
            daily_submenu
        else
            reconf_daily_desktop; settings_daily_desktop_menu
        fi
    elif [[ $BOX == "$SETUP_SERVICES" ]]; then
        settings_daily_service_menu
    elif [[ $BOX == "$SYSTEM_TWEAKS" ]]; then
        settings_daily_tweaks_menu
    elif [[ $BOX == "$GOVERNOR" ]]; then
        settings_daily_governor_menu
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        exec recbox-workflow-settings.sh -sS
    fi
}

studio_submenu() {
BOX=$($SETTINGS_SUB_MENU --title="$STUDIO_SUBMENU_TITLE" --text="$STUDIO_SUBMENU_TEXT" \
    "$SETUP_DESKTOP" "$SEPARATOR" "$SETUP_DESKTOP_DESC" \
    "$SETUP_SERVICES" "$SEPARATOR" "$SETUP_SERVICES_DESC" \
    "$SYSTEM_TWEAKS" "$SEPARATOR" "$SYSTEM_TWEAKS_DESC" \
    "$GOVERNOR" "$SEPARATOR" "$GOVERNOR_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$SETUP_DESKTOP" ]]; then
        if [[ -z $(ls "$HOME/.config/autostart") ]]; then
            zenity --info  --width="400" --title="$AUTOSTART_INFO_TITLE" \
            --text="$AUTOSTART_INFO_TEXT" --icon-name="recbox-workflow-settings" \
            --window-icon="$ICON_PATH"
            studio_submenu
        else
            reconf_studio_desktop; settings_studio_desktop_menu
        fi
    elif [[ $BOX == "$SETUP_SERVICES" ]]; then
        settings_studio_service_menu
    elif [[ $BOX == "$SYSTEM_TWEAKS" ]]; then
        settings_studio_tweaks_menu
    elif [[ $BOX == "$GOVERNOR" ]]; then
        settings_studio_governor_menu
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        exec recbox-workflow-settings.sh -sS
    fi
}

mode_test() {
    if [[ $(grep -w 'mode' "$CONF_PATH" | awk -F '=' '{print $2}') == "daily" ]]; then
        recbox-workflow-desktop.sh -d
    elif [[ $(grep -w 'mode' "$CONF_PATH" | awk -F '=' '{print $2}') == "studio" ]]; then
        recbox-workflow-desktop.sh -s
    fi
}

settings_daily_desktop_menu() {
TRUE=$(grep -w 'false' "$DAILY_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}' | sed 's/^/FALSE /')
FALSE=$(grep -w 'true' "$DAILY_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}' | sed 's/^/TRUE /')
BOX=$($DESKTOP_MENU --title="$DAILY_DESKTOP_MENU_TITLE" --text="$DAILY_DESKTOP_MENU_TEXT" $FALSE $TRUE --separator="\n")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        daily_submenu
    elif [[ $BOX ]]; then
        # Set all options to true (dirty way to set options to true)
        sed -i 's/true/false/' "$DAILY_CONFIG_DESKTOP_PATH"
        # Seting options marked by user to false
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$DAILY_CONFIG_DESKTOP_PATH"
        done
        daily_submenu
    fi
}

settings_studio_desktop_menu() {
TRUE=$(grep -w 'false' "$STUDIO_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}' | sed 's/^/false /')
FALSE=$(grep -w 'true' "$STUDIO_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}' | sed 's/^/true /')
BOX=$($DESKTOP_MENU --title="$STUDIO_DESKTOP_MENU_TITLE" --text="$DAILY_DESKTOP_MENU_TEXT" $FALSE $TRUE  --separator="\n")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        studio_submenu
    elif [[ $BOX ]]; then
        # Set all options to true (dirty way to set options to true)
        sed -i 's/true/false/' "$STUDIO_CONFIG_DESKTOP_PATH"
        # Seting options marked by user to false
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$STUDIO_CONFIG_DESKTOP_PATH"
        done
        studio_submenu
    fi
}

settings_xdg_menu() {
if [[ $(grep -w 'unlock_xdg' "$CONF_PATH" | awk -F '=' '{print $2}') == "true" ]]; then
    TEST_1="TRUE"
    TEST_2="FALSE"
elif [[ $(grep -w 'unlock_xdg' "$CONF_PATH" | awk -F '=' '{print $2}') == "false" ]]; then
    TEST_1="FALSE"
    TEST_2="TRUE"
fi
BOX=$($XDG_MENU --title="$XDG_MENU_TITLE" --text="$XDG_MENU_TEXT" \
    $TEST_1 "$AUTOSTART_OPTION_1" $TEST_2 "$AUTOSTART_OPTION_2")
    if [[ $BOX == "$AUTOSTART_OPTION_1" ]]; then
        sed -i "/unlock_xdg/ s/false/true/" "$CONF_PATH"
        add_xdg_items
        exec recbox-workflow-settings.sh -sS
    elif [[ $BOX == "$AUTOSTART_OPTION_2" ]]; then
        sed -i "/unlock_xdg/ s/true/false/" "$CONF_PATH"
        exec recbox-workflow-settings.sh -sS
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        exec recbox-workflow-settings.sh -sS
    fi
}

settings_daily_service_menu() {
BOX=$($SERVICE_MENU --title="$DAILY_SERVICE_MENU_TITLE" --text="$DAILY_SERVICE_MENU_TEXT" --separator="\n" \
    $(grep -w 'bluetooth.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "bluetooth.service" "$SEPARATOR" "$BLUETOOTH_SERVICE_OPTION" "$SEPARATOR" "$BLUETOOTH_SERVICE_DESC" \
    $(grep -w 'cups.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cups.service" "$SEPARATOR" "$CUPS_SERVICE_OPTION" "$SEPARATOR" "$CUPS_SERVICE_DESC" \
    $(grep -w 'cpupower.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cpupower.service" "$SEPARATOR" "$CPUPOWER_SERVICE_OPTION" "$SEPARATOR" "$CPUPOWER_SERVICE_DESC" \
    $(grep -w 'tlp.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "tlp.service" "$SEPARATOR" "$TLP_SERVICE_OPTION" "$SEPARATOR" "$TLP_SERVICE_DESC" \
    $(grep -w 'hpet.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "hpet.service" "$SEPARATOR" "$HPET_SERVICE_OPTION" "$SEPARATOR" "$HPET_SERVICE_DESC" \
    $(grep -w 'rtc.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "rtc.service" "$SEPARATOR" "$RTC_SERVICE_OPTION" "$SEPARATOR" "$RTC_SERVICE_DESC" \
    $(grep -w 'ufw.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ufw.service" "$SEPARATOR" "$UFW_SERVICE_OPTION" "$SEPARATOR" "$UFW_SERVICE_DESC" \
    $(grep -w 'firewalld.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "firewalld.service" "$SEPARATOR" "$FIREWALLD_SERVICE_OPTION" "$SEPARATOR" "$FIREWALLD_SERVICE_DESC" \
    $(grep -w 'libvirtd.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "libvirtd.service" "$SEPARATOR" "$VIRTMANAGER_SERVICE_OPTION" "$SEPARATOR" "$VIRTMANAGER_SERVICE_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        daily_submenu
    elif [[ $BOX ]]; then
        # Restart config file
        sed -i 's/true/false/' "$DAILY_CONFIG_SERVICE_PATH"
        # Add new configuration
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$DAILY_CONFIG_SERVICE_PATH"
        done
        # cpupower and tlp service check
        TLP=$(grep -w 'tlp.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
        CPUPOWER=$(grep -w 'cpupower.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
        if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
            zenity --info --title="$DAILY_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_1" \
            --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
            settings_daily_service_menu
        else
            # ufw and firewalld service check
            UFW=$(grep -w 'ufw.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
            FIREWALLD=$(grep -w 'firewalld.service' "$DAILY_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
            if [[ $UFW == "true" ]] && [[ $FIREWALLD == "true" ]]; then
                zenity --info --title="$DAILY_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_2" \
                --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
                settings_daily_service_menu
            else
                daily_submenu
            fi
        fi
    fi
}

settings_studio_service_menu() {
BOX=$($SERVICE_MENU --title="$STUDIO_SERVICE_MENU_TITLE" --text="$STUDIO_SERVICE_MENU_TEXT" --separator="\n" \
    $(grep -w 'bluetooth.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "bluetooth.service" "$SEPARATOR" "$BLUETOOTH_SERVICE_OPTION" "$SEPARATOR" "$BLUETOOTH_SERVICE_DESC" \
    $(grep -w 'cups.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cups.service" "$SEPARATOR" "$CUPS_SERVICE_OPTION" "$SEPARATOR" "$CUPS_SERVICE_DESC" \
    $(grep -w 'cpupower.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cpupower.service" "$SEPARATOR" "$CPUPOWER_SERVICE_OPTION" "$SEPARATOR" "$CPUPOWER_SERVICE_DESC" \
    $(grep -w 'tlp.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "tlp.service" "$SEPARATOR" "$TLP_SERVICE_OPTION" "$SEPARATOR" "$TLP_SERVICE_DESC" \
    $(grep -w 'hpet.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "hpet.service" "$SEPARATOR" "$HPET_SERVICE_OPTION" "$SEPARATOR" "$HPET_SERVICE_DESC" \
    $(grep -w 'rtc.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "rtc.service" "$SEPARATOR" "$RTC_SERVICE_OPTION" "$SEPARATOR" "$RTC_SERVICE_DESC" \
    $(grep -w 'ufw.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ufw.service" "$SEPARATOR" "$UFW_SERVICE_OPTION" "$SEPARATOR" "$UFW_SERVICE_DESC" \
    $(grep -w 'firewalld.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "firewalld.service" "$SEPARATOR" "$FIREWALLD_SERVICE_OPTION" "$SEPARATOR" "$FIREWALLD_SERVICE_DESC" \
    $(grep -w 'libvirtd.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "libvirtd.service" "$SEPARATOR" "$VIRTMANAGER_SERVICE_OPTION" "$SEPARATOR" "$VIRTMANAGER_SERVICE_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        studio_submenu
    elif [[ $BOX ]]; then
        # Restart config file
        sed -i 's/true/false/' "$STUDIO_CONFIG_SERVICE_PATH"
        # Add new configuration
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$STUDIO_CONFIG_SERVICE_PATH"
        done
        # cpupower and tlp service check
        TLP=$(grep -w 'tlp.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
        CPUPOWER=$(grep -w 'cpupower.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
        if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
            zenity --info --title="$STUDIO_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_1" \
            --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
            settings_studio_service_menu
        else
            # ufw and firewalld service check
            UFW=$(grep -w 'ufw.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
            FIREWALLD=$(grep -w 'firewalld.service' "$STUDIO_CONFIG_SERVICE_PATH" | awk -F '=' '{print $2}')
            if [[ $UFW == "true" ]] && [[ $FIREWALLD == "true" ]]; then
                zenity --info --title="$STUDIO_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_2" \
                --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
                settings_studio_service_menu
            else
                studio_submenu
            fi
        fi
    fi
}

settings_daily_tweaks_menu() {
BOX=$($TWEAKS_MENU --title="$DAILY_TWEAKS_MENU_TITLE" --text="$DAILY_TWEAKS_MENU_TEXT" --separator="\n" \
    $(grep -w 'audio_group' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "audio_group" "$SEPARATOR" "$AUDIO_GROUP_OPTION" "$SEPARATOR" "$AUDIO_GROUP_DESC" \
    $(grep -w 'realtime_group' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "realtime_group" "$SEPARATOR" "$REALTIME_GROUP_OPTION" "$SEPARATOR" "$REALTIME_GROUP_DESC" \
    $(grep -w 'swappiness' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "swappiness" "$SEPARATOR" "$SWAPPINESS_OPTION" "$SEPARATOR" "$SWAPPINESS_DESC" \
    $(grep -w 'maximum_watches' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "maximum_watches" "$SEPARATOR" "$MAXIMUM_WATCHES_OPTION" "$SEPARATOR" "$MAXIMUM_WATCHES_DESC" \
    $(grep -w 'frequency_scaling_cpu' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_cpu" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_DESC" \
    $(grep -w 'frequency_scaling_tlp' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_tlp" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_DESC" \
    $(grep -w 'threadirqs' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "threadirqs" "$SEPARATOR" "$THREADIRQS_OPTION" "$SEPARATOR" "$THREADIRQS_DESC" \
    $(grep -w 'unlock_memory' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "unlock_memory" "$SEPARATOR" "$UNLOCK_MEMORY_OPTION" "$SEPARATOR" "$UNLOCK_MEMORY_DESC" \
    $(grep -w 'open_files_limit' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "open_files_limit" "$SEPARATOR" "$OPEN_FILES_LIMIT_OPTION" "$SEPARATOR" "$OPEN_FILES_LIMIT_DESC" \
    $(grep -w 'precision_event_timer' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "precision_event_timer" "$SEPARATOR" "$PRECISION_EVENT_TIMER_OPTION" "$SEPARATOR" "$PRECISION_EVENT_TIMER_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        daily_submenu
    elif [[ $BOX ]]; then
        # Restart config file
        sed -i 's/true/false/' "$DAILY_SYSTEM_TWEAKS_PATH"
        # Add new configuration
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$DAILY_SYSTEM_TWEAKS_PATH"
        done
        # Governor menu
        TLP=$(grep -w 'frequency_scaling_tlp' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}')
        CPUPOWER=$(grep -w 'frequency_scaling_cpu' "$DAILY_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}')
        if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
            zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_2" \
            --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
            settings_daily_tweaks_menu
        elif [[ $TLP == "false" ]] && [[ $CPUPOWER == "false" ]]; then
            zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_1" \
            --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
            settings_daily_tweaks_menu
        elif [[ $TLP == "true" ]]; then
            daily_submenu
        elif [[ $CPUPOWER == "true" ]]; then
            daily_submenu
        else
            daily_submenu
        fi
    fi
}

settings_studio_tweaks_menu() {
BOX=$($TWEAKS_MENU --title="$STUDIO_TWEAKS_MENU_TITLE" --text="$STUDIO_TWEAKS_MENU_TEXT" --separator="\n" \
    $(grep -w 'audio_group' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "audio_group" "$SEPARATOR" "$AUDIO_GROUP_OPTION" "$SEPARATOR" "$AUDIO_GROUP_DESC" \
    $(grep -w 'realtime_group' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "realtime_group" "$SEPARATOR" "$REALTIME_GROUP_OPTION" "$SEPARATOR" "$REALTIME_GROUP_DESC" \
    $(grep -w 'swappiness' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "swappiness" "$SEPARATOR" "$SWAPPINESS_OPTION" "$SEPARATOR" "$SWAPPINESS_DESC" \
    $(grep -w 'maximum_watches' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "maximum_watches" "$SEPARATOR" "$MAXIMUM_WATCHES_OPTION" "$SEPARATOR" "$MAXIMUM_WATCHES_DESC" \
    $(grep -w 'frequency_scaling_cpu' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_cpu" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_DESC" \
    $(grep -w 'frequency_scaling_tlp' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_tlp" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_DESC" \
    $(grep -w 'threadirqs' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "threadirqs" "$SEPARATOR" "$THREADIRQS_OPTION" "$SEPARATOR" "$THREADIRQS_DESC" \
    $(grep -w 'unlock_memory' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "unlock_memory" "$SEPARATOR" "$UNLOCK_MEMORY_OPTION" "$SEPARATOR" "$UNLOCK_MEMORY_DESC" \
    $(grep -w 'open_files_limit' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "open_files_limit" "$SEPARATOR" "$OPEN_FILES_LIMIT_OPTION" "$SEPARATOR" "$OPEN_FILES_LIMIT_DESC" \
    $(grep -w 'precision_event_timer' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "precision_event_timer" "$SEPARATOR" "$PRECISION_EVENT_TIMER_OPTION" "$SEPARATOR" "$PRECISION_EVENT_TIMER_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        studio_submenu
    elif [[ $BOX ]]; then
        # Restart config file
        sed -i 's/true/false/' "$STUDIO_SYSTEM_TWEAKS_PATH"
        # Add new configuration
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$STUDIO_SYSTEM_TWEAKS_PATH"
        done
        # Governor menu
        TLP=$(grep -w 'frequency_scaling_tlp' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}')
        CPUPOWER=$(grep -w 'frequency_scaling_cpu' "$STUDIO_SYSTEM_TWEAKS_PATH" | awk -F '=' '{print $2}')
        if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
            zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_2" \
            --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
            settings_studio_tweaks_menu
        elif [[ $TLP == "false" ]] && [[ $CPUPOWER == "false" ]]; then
            zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_1" \
            --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
            settings_studio_tweaks_menu
        elif [[ $TLP == "true" ]]; then
            studio_submenu
        elif [[ $CPUPOWER == "true" ]]; then
            studio_submenu
        else
            studio_submenu
        fi
    fi
}

settings_daily_governor_menu() {
BOX=$($GOVERNOR_MENU --title="$GOV_MENU_TITLE" --text="$GOV_MENU_TEXT" --separator="\n" \
    $(grep -w 'gov_ondemand' "$DAILY_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ondemand" "$SEPARATOR" "$ONDEMAND_OPTION" "$SEPARATOR" "$ONDEMAND_DESC" \
    $(grep -w 'gov_powersave' "$DAILY_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "powersave" "$SEPARATOR" "$POWERSAVE_OPTION" "$SEPARATOR" "$POWERSAVE_DESC" \
    $(grep -w 'gov_userspace' "$DAILY_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "userspace" "$SEPARATOR" "$USERSPACE_OPTION" "$SEPARATOR" "$USERSPACE_DESC" \
    $(grep -w 'gov_performance' "$DAILY_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "performance" "$SEPARATOR" "$PERFORMANCE_OPTION" "$SEPARATOR" "$PERFORMANCE_DESC" \
    $(grep -w 'gov_conservative' "$DAILY_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "conservative" "$SEPARATOR" "$CONSERVATIVE_OPTION" "$SEPARATOR" "$CONSERVATIVE_DESC" \
    $(grep -w 'gov_schedutil' "$DAILY_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "schedutil" "$SEPARATOR" "$SCHEDUTIL_OPTION" "$SEPARATOR" "$SCHEDUTIL_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        daily_submenu
    elif [[ $BOX ]]; then
        # Restart config file
        sed -i 's/true/false/' "$DAILY_GOVERNOR_PATH"
        # Add new configuration
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$DAILY_GOVERNOR_PATH"
        done
        daily_submenu
    fi
}

settings_studio_governor_menu() {
BOX=$($GOVERNOR_MENU --title="$GOV_MENU_TITLE" --text="$GOV_MENU_TEXT" --separator="\n" \
$(grep -w 'gov_ondemand' "$STUDIO_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ondemand" "$SEPARATOR" "$ONDEMAND_OPTION" "$SEPARATOR" "$ONDEMAND_DESC" \
$(grep -w 'gov_powersave' "$STUDIO_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "powersave" "$SEPARATOR" "$POWERSAVE_OPTION" "$SEPARATOR" "$POWERSAVE_DESC" \
$(grep -w 'gov_userspace' "$STUDIO_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "userspace" "$SEPARATOR" "$USERSPACE_OPTION" "$SEPARATOR" "$USERSPACE_DESC" \
$(grep -w 'gov_performance' "$STUDIO_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "performance" "$SEPARATOR" "$PERFORMANCE_OPTION" "$SEPARATOR" "$PERFORMANCE_DESC" \
$(grep -w 'gov_conservative' "$STUDIO_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "conservative" "$SEPARATOR" "$CONSERVATIVE_OPTION" "$SEPARATOR" "$CONSERVATIVE_DESC" \
$(grep -w 'gov_schedutil' "$STUDIO_GOVERNOR_PATH" | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "schedutil" "$SEPARATOR" "$SCHEDUTIL_OPTION" "$SEPARATOR" "$SCHEDUTIL_DESC")
    if [[ -z $BOX ]]; then
        exit 0
    elif [[ $BOX == "$EXTRA_BUTTON" ]]; then
        studio_submenu
    elif [[ $BOX ]]; then
        # Restart config file
        sed -i 's/true/false/' "$STUDIO_GOVERNOR_PATH"
        # Add new configuration
        for i in $BOX; do
            sed -i "/$i/ s/false/true/" "$STUDIO_GOVERNOR_PATH"
        done
        studio_submenu
    fi
}

sync_configs() {
# Autostart configuration when file doesn't exist or empty.
if [[ -d $HOME/.config/recbox-workflow-settings ]]; then
    echo ":: Config directory exist"
elif [[ ! -d $HOME/.config/recbox-workflow-settings ]]; then
    echo ":: Config directory doesn't exist"
    mkdir "$HOME/.config/recbox-workflow-settings"
fi
# Main config file items test
if [[ -s $CONF_PATH ]]; then
    if [[ -z $(grep -w 'mode' "$CONF_PATH") ]]; then
        echo -e ":: 'mode' option don't exist\nadding item to file"
        echo "mode=daily" >> "$CONF_PATH"
    elif [[ ! -z $(grep -w 'mode' "$CONF_PATH") ]]; then
        echo ":: 'mode' item exist in main config, skipping"
    fi
    if [[ -z $(grep -w 'unlock_xdg' "$CONF_PATH") ]]; then
        echo -e ":: 'unlock_xdg' option don't exist\nadding item to file"
        echo "unlock_xdg=false" >> "$CONF_PATH"
    elif [[ ! -z $(grep -w 'unlock_xdg' "$CONF_PATH") ]]; then
        echo ":: 'unlock_xdg' item exist in main config, skipping"
    fi
elif [[ ! -s $HOME/.config/recbox-workflow-settings/config ]]; then
    echo ":: Main config file is empty, adding items"
    echo -e "mode=daily\nunlock_xdg=false" > "$CONF_PATH"
fi
# Session and Startup
if [[ ! -f $DAILY_CONFIG_DESKTOP_PATH ]]; then
    echo ":: $AUTOSTART_OUTPUT_CREATING_CONFIG_DAILY"; reconf_daily_desktop
fi
if [[ ! -f $STUDIO_CONFIG_DESKTOP_PATH ]]; then
    echo ":: $AUTOSTART_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_studio_desktop
fi
# Services
if [[ -f $DAILY_CONFIG_SERVICE_PATH ]]; then
    if [[ -s $DAILY_CONFIG_SERVICE_PATH ]]; then
        echo ":: $SERVICE_OUTPUT_CONFIGURED_DAILY"
    else
        echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $SERVICE_OUTPUT_CREATING_CONFIG_DAILY"; reconf_daily_services
    fi
else
    echo ":: $SERVICE_OUTPUT_CREATING_CONFIG_DAILY"; reconf_daily_services
fi
if [[ -f $STUDIO_CONFIG_SERVICE_PATH ]]; then
    if [[ -s $STUDIO_CONFIG_SERVICE_PATH ]]; then
        echo ":: $SERVICE_OUTPUT_CONFIGURED_STUDIO"
    else
        echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $SERVICE_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_studio_services
    fi
else
    echo ":: $SERVICE_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_studio_services
fi
# System tweaks
if [[ -f $DAILY_SYSTEM_TWEAKS_PATH ]]; then
    if [[ -s $DAILY_SYSTEM_TWEAKS_PATH ]]; then
        echo ":: $TWEAKS_OUTPUT_CONFIGURED_DAILY"
    else
        echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $TWEAKS_OUTPUT_CREATING_CONFIG_DAILY"; reconf_daily_tweaks; reconf_daily_governor
    fi
else
    echo ":: $TWEAKS_OUTPUT_CREATING_CONFIG_DAILY"; reconf_daily_tweaks; reconf_daily_governor
fi
if [[ -f $STUDIO_SYSTEM_TWEAKS_PATH ]]; then
    if [[ -s $STUDIO_SYSTEM_TWEAKS_PATH ]]; then
        echo ":: $TWEAKS_OUTPUT_CONFIGURED_STUDIO"
    else
        echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $TWEAKS_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_studio_tweaks; reconf_studio_governor
    fi
else
    echo ":: $TWEAKS_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_studio_tweaks; reconf_studio_governor
fi
}

add_xdg_items() {
mkdir -p /tmp/workflow-settings/
cp /etc/xdg/autostart/* /tmp/workflow-settings/
REMOVE_ITEMS=$(grep -wE 'Hidden' /tmp/workflow-settings/*desktop | awk -F ':' '{print $1}')
    for i in $REMOVE_ITEMS; do
        cp -n $i $HOME/.config/autostart/
        rm $i
    done
CONF_ITEMS=$(ls /tmp/workflow-settings/*desktop)
    for i in $CONF_ITEMS; do
        echo "Hidden=false" | tee -a $i
        cp -n $i $HOME/.config/autostart/
    done  1> /dev/null
rm -r /tmp/workflow-settings
}

reconf_daily_desktop() {
# Create autostart items from /etc/xdg/autostart
if [[ $(grep -w 'unlock_xdg' "$CONF_PATH" | awk -F '=' '{print $2}') == "true" ]]; then
    add_xdg_items
fi
if [[ -z $(ls "$HOME/.config/autostart") ]]; then
    echo ":: Directory empty, skipping"
else
    # Create new config file and set all to 'true'
    grep -wE 'Hidden' $HOME/.config/autostart/*.desktop | awk -F '/' '{print $6}' | sed 's/:Hidden//' > "$DAILY_CONFIG_DESKTOP_PATH".new
    # Combine config files
    TRUE_ITEM=$(grep -w 'true' "$DAILY_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}')
    FALSE_ITEM=$(grep -w 'false' "$DAILY_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}')
        for i in $TRUE_ITEM; do
            sed -i "/$i/ s/false/true/" "$DAILY_CONFIG_DESKTOP_PATH".new
        done
        for i in $FALSE_ITEM; do
            sed -i "/$i/ s/true/false/" "$DAILY_CONFIG_DESKTOP_PATH".new
        done
        mv "$DAILY_CONFIG_DESKTOP_PATH".new "$DAILY_CONFIG_DESKTOP_PATH"
fi
}

reconf_studio_desktop() {
# Create autostart items from /etc/xdg/autostart
if [[ $(grep -w 'unlock_xdg' "$CONF_PATH" | awk -F '=' '{print $2}') == "true" ]]; then
    add_xdg_items
fi
if [[ -z $(ls "$HOME/.config/autostart") ]]; then
    echo ":: Directory empty, skipping"
else
    # Create autostart items from /etc/xdg/autostart
    if [[ $(grep -w 'unlock_xdg' "$CONF_PATH" | awk -F '=' '{print $2}') == "true" ]]; then
        add_xdg_items
    fi
    # Create new config file and set all to 'true'
    grep -wE 'Hidden' $HOME/.config/autostart/*.desktop | awk -F '/' '{print $6}' | sed 's/:Hidden//' > "$STUDIO_CONFIG_DESKTOP_PATH".new
    # Combine config files
    TRUE_ITEM=$(grep -w 'true' "$STUDIO_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}')
    FALSE_ITEM=$(grep -w 'false' "$STUDIO_CONFIG_DESKTOP_PATH" | awk -F '=' '{print $1}')
        for i in $TRUE_ITEM; do
            sed -i "/$i/ s/false/true/" "$STUDIO_CONFIG_DESKTOP_PATH".new
        done; echo $TRUE_ITEM
        for i in $FALSE_ITEM; do
            sed -i "/$i/ s/true/false/" "$STUDIO_CONFIG_DESKTOP_PATH".new
        done
        mv "$STUDIO_CONFIG_DESKTOP_PATH".new "$STUDIO_CONFIG_DESKTOP_PATH"
fi
}

reconf_daily_services(){
echo -e "bluetooth.service=true\ncups.service=false\ntlp.service=false\ncpupower.service=false\nhpet.service=false\nrtc.service=false\nufw.service=true\nfirewalld.service=false\nlibvirtd.service=false" > "$DAILY_CONFIG_SERVICE_PATH"
}

reconf_studio_services(){
echo -e "bluetooth.service=false\ncups.service=false\ntlp.service=false\ncpupower.service=false\nhpet.service=false\nrtc.service=false\nufw.service=false\nfirewalld.service=false\nlibvirtd.service=false" > "$STUDIO_CONFIG_SERVICE_PATH"
}

reconf_daily_tweaks() {
echo -e "audio_group=true\nrealtime_group=false\nswappiness=false\nmaximum_watches=false\nfrequency_scaling_tlp=false\nfrequency_scaling_cpu=false\nthreadirqs=false\nunlock_memory=false\nopen_files_limit=false\nprecision_event_timer=false" > "$DAILY_SYSTEM_TWEAKS_PATH"
}

reconf_studio_tweaks() {
echo -e "audio_group=true\nrealtime_group=true\nswappiness=true\nmaximum_watches=true\nfrequency_scaling_tlp=false\nfrequency_scaling_cpu=false\nthreadirqs=true\nunlock_memory=true\nopen_files_limit=true\nprecision_event_timer=true" > "$STUDIO_SYSTEM_TWEAKS_PATH"
}

reconf_daily_governor() {
echo -e "gov_ondemand=false\ngov_powersave=false\ngov_userspace=false\ngov_performance=false\ngov_conservative=false\ngov_schedutil=true" > "$DAILY_GOVERNOR_PATH"
}

reconf_studio_governor() {
echo -e "gov_ondemand=false\ngov_powersave=false\ngov_userspace=false\ngov_performance=true\ngov_conservative=false\ngov_schedutil=false" > "$STUDIO_GOVERNOR_PATH"
}

error_dialog() {
zenity --error --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_SYNC_TEXT" \
--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --width="400"
}

case "$1" in
    --settings | -S ) settings;;
    --sync | -s ) sync_configs;;
    -sS | -Ss | -ss | -SS ) sync_configs && settings || error_dialog;;
    --daily-submenu ) daily_submenu;;
    --studio-submenu ) studio_submenu;;
    --daily-desktop-submenu ) settings_daily_desktop_menu;;
    --studio-desktop-submenu ) settings_studio_desktop_menu;;
    --daily-services-submenu ) settings_daily_service_menu;;
    --studio-services-submenu ) settings_studio_service_menu;;
    --daily-tweaks-submenu ) settings_daily_tweaks_menu;;
    --studio-tweaks-submenu ) settings_studio_tweaks_menu;;
    --daily-governor-menu ) settings_daily_governor_menu;;
    --studio-governor-menu ) settings_studio_governor_menu;;
    --reconf-daily-desktop ) reconf_daily_desktop;;
    --reconf-studio-desktop ) reconf_studio_desktop;;
    --restart-configs | -r ) reconf_daily_desktop; reconf_studio_desktop; reconf_daily_services; reconf_studio_services; reconf_daily_tweaks; reconf_studio_tweaks; reconf_daily_governor; reconf_studio_governor; exec recbox-workflow-settings.sh -s;;
    --version | -v ) echo -e "\n    Version $VER\n";;
    *)
echo -e "
    Usage:\n
    recbox-workflow-settings.sh [ OPTION ]\n
    Options:\n
    --settings, -S                          settings - main menu
    --sync, -s                              syncronize config files
    -sS, -Ss, -ss, -SS                      syncronize config files
                                            and settings mein menu
    --daily-submenu                         Daily submenu
    --studio-submenu                        Studio submenu
    --daily-desktop-submenu                 Daily desktop submenu
    --studio-desktop-submenu                Studio desktop submenu
    --daily-services-submenu                Daily service submenu
    --studio-services-submenu               Studio service submenu
    --daily-tweaks-submenu                  Daily tweaks submenu
    --studio-tweaks-submenu                 Studio tweaks menu
    --daily-governor-menu                   Daily TLP governor submenu
    --studio-governor-menu                  Studio TLP governor submenu
    --daily-cpupower-scaling-submenu        Daily CPU Power governor submenu
    --studio-cpupower-scaling-submenu       Studio CPU governor submenu
    --restart-configs, -r                   reconfigure Daily services config
                                            reconfigure Studio services config
                                            reconfigure Daily tweaks config
                                            reconfigure Studio tweaks config

    --version, -v                           Workflow Settings version
" >&2
exit 1;;
esac
exit 0
