#!/usr/bin/env bash

SYS_LANG=$(echo "$LANG" | cut -d '.' -f 1)
if [[ -z $(find /usr/share/recbox-workflow/translations/system/ -name "$SYS_LANG".trans) ]]; then
	source /usr/share/recbox-workflow/translations/system/en_US.trans
else
	for TRANS in /usr/share/recbox-workflow/translations/system/"$SYS_LANG"*; do
		source $TRANS
	done
fi
